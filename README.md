# Personal Portfolio

Personal portfolio website made with django and react

# Set up

Update from db: 
`python manage.py inspectdb > models.py`

`python manage.py makemigrations`

`python manage.py migrate`

run: 
`python myproject/manage.py runserver 0.0.0.0:8000`

export data:
`python manage.py dumpdata > ../portfolio_data.json`

Open up browser to localhost:8000/portfolio_app/
