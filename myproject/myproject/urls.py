from cgitb import handler
from django.contrib import admin
from django.urls import path
from myApp import views
from account.views import createuser, signout, signin
from django.conf.urls.static import static
from django.conf import settings



urlpatterns = [
    path('', views.home, name='home'),
    path('admin/', admin.site.urls),
    path('createuser', createuser, name="createuser"),
    path("signout", signout, name="signout"),
    path("signin", signin, name="signin"),
    path('modify_user_info', views.modify_user_info, name="modify_user_info"),
    path('modify_education', views.EducationFormView.as_view(), name="modify_education"),
    path('modify_experience', views.ExperienceFormView.as_view(), name="modify_experience"),
    path('modify_projects', views.ProjectFormView.as_view(), name="modify_projects"),
    path('modify_skills', views.SkillFormView.as_view(), name="modify_skills"),
    path("<str:username>", views.home_aux, name="home"),
       
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = "myApp.views.page_not_found"
handler500 = "myApp.views.error_page"
