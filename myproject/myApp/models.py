# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from account.models import User

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/beat/author/<filename>
    return 'media/{0}/{1}'.format(instance.username, filename)

def project_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/beat/author/<filename>
    return 'media/{0}/projects/{1}'.format(instance.username, filename)

class Education(models.Model):
    username = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    degree_type = models.CharField(max_length=20)
    program_name = models.CharField(max_length=45)
    institution = models.CharField(max_length=45)
    year_completed = models.IntegerField(blank=True, null=True)
    year_started = models.IntegerField(blank=True, null=True)
    gpa = models.FloatField(blank=True, null=True)
    description = models.CharField(blank=True, null=True, max_length=400)

    class Meta:
        db_table = 'education'
        unique_together = (('username' , 'program_name', 'degree_type'),)


class Experience(models.Model):
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=45)
    company = models.CharField(max_length=45)
    start_month = models.CharField(max_length=10)
    start_year = models.IntegerField()
    b1 = models.CharField(max_length=200, blank=True, null=True)
    b2 = models.CharField(max_length=200, blank=True, null=True)
    b3 = models.CharField(max_length=200, blank=True, null=True)
    b4 = models.CharField(max_length=200, blank=True, null=True)
    end_month = models.CharField(max_length=45, blank=True, null=True)
    end_year = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'experience'
        unique_together = (('username' , 'company', 'start_month', 'start_year', 'title'),)


class Project(models.Model):
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=45)
    month = models.CharField(max_length=10)
    year = models.IntegerField()
    description = models.CharField(max_length=400, blank=True, null=True)
    link = models.CharField(max_length=45, blank=True, null=True)
    tech_stack = models.CharField(max_length=100, blank=True, null=True)
    picture = models.FileField(upload_to=project_directory_path, null=True, blank=True)

    class Meta:
        db_table = 'projects'
        unique_together = (('username' , 'title'),)


class Skill(models.Model):
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    skill_name = models.CharField(max_length=15)
    years_practiced = models.IntegerField()
    description = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'skills'
        unique_together = (('username' , 'skill_name'),)


class UserInfo(models.Model):
    username = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    first = models.CharField(max_length=15, blank=True, null=True)
    last = models.CharField(max_length=15, blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    website = models.CharField(max_length=30, blank=True, null=True)
    linkedin = models.CharField(max_length=30, blank=True, null=True)
    git = models.CharField(max_length=30, blank=True, null=True)
    tagline = models.CharField(max_length=45, blank=True, null=True)
    summary = models.CharField(max_length=500, blank=True, null=True)
    picture = models.FileField(upload_to=user_directory_path, null=True, blank=True)

    class Meta:
        db_table = 'user_info'



