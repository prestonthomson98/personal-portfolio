from curses import meta
from curses.ascii import US
from dataclasses import field, fields
from pyexpat import model
from turtle import title
from django.forms import Form, ModelForm, ImageField
from django.contrib.auth.forms import UserCreationForm
from .models import Project, Skill, Experience, Education, UserInfo
from django import forms
from account.models import User

class UserInfoForm(ModelForm):
    class Meta:
        model = UserInfo
        exclude = ['username']


class SkillForm(ModelForm):
    title = "Skill"
    class Meta:
        model = Skill
        exclude = ["username"]

class ProjectForm(ModelForm):
    title = "Project"
    class Meta: 
        model = Project
        exclude = ["username"]

class ExperienceForm(ModelForm):
    title = "Experience"
    class Meta:
        model = Experience
        exclude = ["username"]

class EducationForm(ModelForm):
    title = "Education"
    class Meta:
        model = Education
        exclude = ["username"]

        
