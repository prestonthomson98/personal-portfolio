from enum import unique
import re
from urllib import request
from django.contrib import messages
from django import forms
from .models import Project, Skill, UserInfo, Experience, Education 
from django.forms import BaseModelFormSet, ModelForm, ValidationError, modelformset_factory
from django.http import HttpRequest
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import FormView
from myApp.models import Education, UserInfo
from account.models import User
from .forms import UserInfoForm, EducationForm, ExperienceForm, ProjectForm, SkillForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.shortcuts import render, redirect
from django.urls import reverse
from django import forms
from django.db import models
import boto3
import os
from account.views import createuser

def page_not_found(request, exception):
    return render(request, "model_not_found.html", status=404)

def error_page(request, exception=None):
    return render(request, "error.html", status=500)


def home(request):
    if not request.user.is_authenticated:
        return createuser(request=request)
    else:
        return home_aux(request, request.user.username)
    
        
def home_aux(request, username):
    try:
        user = User.objects.get(username=username)
    except:
        return render(request, "model_not_found.html")
    context = {}
    try:
        user_info = UserInfo.objects.get(username=user)
        context["user_info"] = user_info
    except:
        return redirect('modify_user_info')
    context["education"] = Education.objects.filter(username=user)
    context["experience"] = Experience.objects.filter(username=user)
    context["projects"] = Project.objects.filter(username=user)
    context["skills"] = Skill.objects.filter(username=user)
    print(context) 
    return render(request, "index.html", context)

def modify_user_info(request: HttpRequest):
    if request.method == 'POST':
        form = UserInfoForm(request.POST, request.FILES)
        if form.is_valid():
            info = form.save(commit=False)
            print(info.picture)
            user = User.objects.get(username=request.user.username)
            info.username = user
            info.save()
            redir = "home"
            if request.POST.get("submit_option") == "Modify Education Info":
                redir = "modify_education"
                return redirect(redir)
            return redirect(redir)
    if request.method == 'GET':
        try:
            user = User.objects.get(username=request.user.username)
        except:
            return page_not_found(request, None)
        try:
            form = UserInfoForm(instance=UserInfo.objects.get(username=request.user))
        except:
            form = UserInfoForm()
    return render(request, "modify_user_info.html", {"form": form})




BaseModelFormSet.original_is_valid = BaseModelFormSet.is_valid

def formset_validate_unique(formset: BaseModelFormSet):
    if not BaseModelFormSet.original_is_valid(formset): return False
    unique_togethers = tuple(x for x in formset.model._meta.unique_together[0] if x != 'username')
    uniques_set = set()
    for form in formset:
        temp_tuple = tuple(form.cleaned_data.get(x) for x in unique_togethers)
        if temp_tuple in uniques_set:
            form.add_error(None, ValidationError("You have duplicate entries in your form.  Fields that must be unique: " \
                + str(unique_togethers)))
            return False
        else:
            uniques_set.add(temp_tuple)
    return True

BaseModelFormSet.is_valid = formset_validate_unique

class EducationFormView(FormView):

    def __init__(self):
        super().__init__()
        self.Education_FormSet=modelformset_factory(Education, EducationForm, can_delete=True)
        self.template_name = "modify_education.html"

    def get(self,request,*args,**kwargs):
    #Creating an Instance of formset and putting it in context dict
        try:
            user = User.objects.get(username=request.user.username)
        except:
            return page_not_found(request)
        education_formset = self.Education_FormSet()
        context={
            'education_form': education_formset,
        }
        return render(request, self.template_name, context)
    
    def post(self,request):
        education_formset=self.Education_FormSet(request.POST)        
        #Checking the if the form is valid
        if education_formset.is_valid():
            objs = Education.objects.filter(username=request.user)
            objs.delete()
            #To save we have loop through the formset
            for form in education_formset:
                info = form.save(commit=False)
                info.username = request.user
                info.save()
            redir = "home"
            if request.POST.get("submit_option") == "Modify Experience Info":
                redir = "modify_experience"
            return redirect(redir)

        else:
            context={
                    'education_form':education_formset,
                    }

            return render(request, self.template_name, context)

class ExperienceFormView(FormView):
    
    def __init__(self):
        super().__init__()
        self.Experience_FormSet=modelformset_factory(Experience, ExperienceForm, can_delete=True, )
        self.template_name = "modify_experience.html"

    def get(self,request,*args,**kwargs):
        try:
            user = User.objects.get(username=request.user.username)
        except:
            return page_not_found(request)
    #Creating an Instance of formset and putting it in context dict
        experience_formset = self.Experience_FormSet()
        context={
            'experience_form': experience_formset,
        }
        return render(request, self.template_name, context)
    
    def post(self,request):
        experience_formset=self.Experience_FormSet(request.POST)

        #Checking the if the form is valid
        if experience_formset.is_valid():
            objs = Experience.objects.filter(username=request.user)
            objs.delete()
            #To save we have loop through the formset
            for form in experience_formset:
                info = form.save(commit=False)
                info.username = request.user
                info.save()
            redir = "home"
            if request.POST.get("submit_option") == "Modify Project Info":
                redir = "modify_projects"
            return redirect(redir)

        else:
            context={
                    'experience_form':experience_formset,
                    }

            return render(request, self.template_name, context)

class ProjectFormView(FormView):
    
    def __init__(self):
        super().__init__()
        self.Project_FormSet=modelformset_factory(Project, ProjectForm)
        self.template_name = "modify_projects.html"

    def get(self,request,*args,**kwargs):
    #Creating an Instance of formset and putting it in context dict
        try:
            user = User.objects.get(username=request.user.username)
        except:
            return page_not_found(request)
        project_formset = self.Project_FormSet()
        context={
            'project_forms': project_formset,
        }
        return render(request, self.template_name, context)
    
    def post(self,request):
        project_formset=self.Project_FormSet(request.POST, request.FILES)

        #Checking the if the form is valid
        if project_formset.is_valid():
            objs = Project.objects.filter(username=request.user)
            objs.delete()
            #To save we have loop through the formset
            for form in project_formset:
                info = form.save(commit=False)
                info.username = request.user
                info.save()
            redir = "home"
            if request.POST.get("submit_option") == "Modify Skills Info":
                redir = "modify_skills"
            return redirect(redir)

        else:
            context={
                    'project_forms':project_formset,
                    }

            return render(request, self.template_name, context)


class SkillFormView(FormView):
    
    def __init__(self):
        super().__init__()
        self.Skill_FormSet=modelformset_factory(Skill, SkillForm, max_num=6)
        self.template_name = "modify_skills.html"

    def get(self,request,*args,**kwargs):
    #Creating an Instance of formset and putting it in context dict
        try:
            user = User.objects.get(username=request.user.username)
        except:
            return page_not_found(request)
        skill_formset = self.Skill_FormSet()
        context={
            'skills_form': skill_formset,
        }
        return render(request, self.template_name, context)
    
    def post(self,request):
        skill_formset=self.Skill_FormSet(request.POST)

        #Checking the if the form is valid
        if skill_formset.is_valid():
            objs = Skill.objects.filter(username=request.user)
            objs.delete()
            #To save we have loop through the formset
            for form in skill_formset:
                info = form.save(commit=False)
                info.username = request.user
                info.save()
            redir = "home"
            return redirect(redir)

        else:
            context={
                    'skills_form':skill_formset,
                    }

            return render(request, self.template_name, context)