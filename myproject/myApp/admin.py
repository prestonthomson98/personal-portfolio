from django.contrib import admin
from .models import UserInfo, Project, Experience, Education, Skill

admin.site.register(Project)
admin.site.register(Experience)
admin.site.register(Education)
admin.site.register(Skill)




