import email
from django.http import HttpRequest
from django.shortcuts import render
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import redirect
from account.forms import RegistrationForm, SignInForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
import requests

# Create your views here.
def createuser(request):
    if request.method == 'POST':
        return create_new_user(request)
    if request.method == 'GET':
        return render(request, "new_user_form.html", {"form": RegistrationForm()})

def create_new_user(request):
    form = RegistrationForm(request.POST)
    if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("modify_user_info")
    else: 
        return render(request, "new_user_form.html", {"form": form})

def signout(request):
    logout(request)
    return redirect("createuser")

def signin(request: HttpRequest):
    if request.user.is_authenticated:
        return redirect("modify_user_info")
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect("modify_user_info")
    if request.method == 'GET':
        form = AuthenticationForm()
    return render(request, "sign_in.html", {"form": form})