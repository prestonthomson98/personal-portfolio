from os import access
from django.contrib import admin
from account.models import User
from myApp.models import UserInfo, Education

# Register your models here.

admin.site.register(User)
admin.site.register(UserInfo)
