from django.contrib.auth.backends import ModelBackend
from django.db.models import Q
from account.models import User

class EmailBackend(ModelBackend):
    def authenticate(self, request, email=None, password=None, **kwargs):
        try:
            User.objects.all()
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            User().set_password(password)
            return
        except User.MultipleObjectsReturned:
            user = User.objects.filter(email=email).order_by('id').first()

        if user.check_password(password) and self.user_can_authenticate(user):
            return user