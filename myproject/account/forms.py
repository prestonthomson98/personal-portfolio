from dataclasses import field, fields
import email
from turtle import title
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate

from account.models import User


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(max_length=60, help_text='Add a valid email address')
    password1 = forms.CharField(widget=forms.PasswordInput(), label='Password')
    password2 = forms.CharField(widget=forms.PasswordInput(), label='Confirm Password')
    class Meta:
        model = User
        fields = ('email', 'username', 'password1', 'password2')

class SignInForm(forms.Form):
    username = forms.EmailField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())        
