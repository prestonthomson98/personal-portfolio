FROM python:3.9
WORKDIR /code
ADD . /code/
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
RUN apt-get update && apt-get install -y gcc && \
    apt-get install -y default-libmysqlclient-dev && \
    pip install -r requirements.txt 

CMD python myproject/manage.py makemigrations && python myproject/manage.py migrate && \
    python myproject/manage.py collectstatic && python myproject/manage.py runserver 0.0.0.0:5000
